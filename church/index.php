<?php
//require_once('bdd.php');
require_once("conex.php");

$conexion = conectar();


$sql = "SELECT id, title, start, end, color FROM events ";

$events = ejecutar($sql,$conexion); 

//$req = $bdd->prepare($sql);
//$req->execute();

//$events = $req->fetchAll();

$sql2  = "SELECT * FROM groups";
$resultado1 = ejecutar($sql2,$conexion); 
$j = 0;      
while($datos = mysqli_fetch_assoc($resultado1)){            
    $groups[$j] = $datos;
    $j++;
}

$sql23  = "SELECT * FROM templates where true order by id desc";
$resultado13 = ejecutar($sql23,$conexion); 
$r = 0;      
while($datos3 = mysqli_fetch_assoc($resultado13)){            
    $template[$r] = $datos3;
    $r++;
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CHURCH</title>

    <!-- Bootstrap Core CSS -->
    

	
	<!-- FullCalendar -->
	
	
 <link href='css/fullcalendar.css' rel='stylesheet' />


    <link href="plugins/select2/select2.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css" type='text/css'/>
    <script src="plugins/jquery/jquery.min.js"></script>
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>    
    <script src="plugins/bootstrap/bootstrap.min.js"></script>
    <script src="plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/bootstrap-multiselect.js"></script>
    <script src="js/listas.js" type="text/javascript" ></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">



    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
	#calendar {
		max-width: 800px;
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  

<!-- NAV BAR  -->
 <?php  include "nav_bar.php"; ?>
    <br><br>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>CHURCH EVENTS</h1>
                <div id="calendar" class="col-centered">
                </div>
            </div>
			
        </div>
        <!-- /.row -->
		
		<!-- Modal -->
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="addEvent.php">
			
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Event</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Title</label>
					<div class="col-sm-10">
					  <input type="text" name="title" class="form-control" id="title" placeholder="Title">
					</div>
				  </div>
				  <div class="form-group">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
						  <option value="">Choose</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Dark blue</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquoise</option>
						  <option style="color:#008000;" value="#008000">&#9724; Green</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Yellow</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Orange</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Red</option>
						  <option style="color:#000;" value="#000">&#9724; Black</option>
						  
						</select>
					</div>
				  </div>
				  <div class="form-group">
					<label for="start" class="col-sm-2 control-label">Start date</label>
					<div class="col-sm-10">
					  <input type="text" name="start" class="form-control" id="start" readonly>
					</div>
				  </div>
				  <div class="form-group">
					<label for="end" class="col-sm-2 control-label">End date</label>
					<div class="col-sm-10">
					  <input type="text" name="end" class="form-control" id="end" readonly>
					</div>
				  </div>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>
		
		
		
		<!-- Modal -->
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="editEventTitle.php">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Event</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Title</label>
					<div class="col-sm-10">
					  <input type="text" name="title" class="form-control" id="title" placeholder="Title">
					</div>
				  </div>
				  <div class="form-group">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
						  <option value="">Choose</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Dark blue</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquoise</option>
						  <option style="color:#008000;" value="#008000">&#9724; Green</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Yellow</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Orange</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Red</option>
						  <option style="color:#000;" value="#000">&#9724; Black</option>
						  
						</select>
					</div>
				  </div>
				    <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						  <div class="checkbox">
							<label class="text-danger"><input type="checkbox"  name="delete"> Delete event</label>
						  </div>
						</div>
					</div>
				  
				  <input type="hidden" name="id" class="form-control" id="id">
				
				
			  </div>
			  <div class="modal-header">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
        </form>
  <!-- MENSAJES -->
        <form id="form1" role="form" action="controlador/sendSms_RingCentral.php" method="post">
		
		 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 align="left" class="modal-title" id="myModalLabel"><font  color="Orange">INVITE PEOPLE TO EVENT</font></h4>
			  </div>
			  
		<div class="row" style="margin : 2px;">
                  
 
                   <div class="row" style="margin : 2px;">
                    
                        <div class="form-group">
                                                           
                            <label><font color="#585858">GROUP</font></label>
                            <div class="row">
                                <div class="col-sm-10">
                                    <select class="form-control" id="id_groups" name="id_groups" onchange="temp(this.value);">
                                    	<option value="0">--- SELECT TYPE PERSON ---</option>
                                    	<?php                                 
                           					$i=0;
                            				while(isset($groups[$i])!=null){
                        				?>
                            				<option value="<?=$groups[$i]['id_groups']?>"><?=$groups[$i]['group_name']?></option>
                        				<?php
                            				$i++;
                            			    }
                        				?>
                                    </select>
                                </div>  
                            </div>
                        </div>

                         <div id="output1"></div>                  
                        <div class="form-group">
                   

			    <label for="inputSubject">Subject</label>
                            <div class="row">
                                <div class="col-sm-10 text-center">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                </div>
                            </div>
                        </div><br>
					 </div>
					 <div id="plantilla">

                            <div class="form-group">

                                <label for="inputSubject">Template</label>
                                <div class="row">
                                    <div class="col-sm-10" id="tipo_persona_select">
                                        <select name="templates" id="templates" class="form-control" onchange="templates_n(this)">
                                            <option value="0">--SELECT--</option>
                                            <option value="Without">Without Template</option>
                                            <option value="nueva">New</option>                                
                                            <?php 
                                                $i=0;
                                                while(isset($template[$i])!=null){
                                            ?>
                                                <option value="<?=$template[$i]['id']?>"><?=$template[$i]['name']?></option>
                                            <?php
                                                $i++;
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                            </div><br>
                    </div>
            <div id="body_sms"></div>
		 
		
		  <div class="row" style="margin : 2px;">
			 <div class="col-xs-2">
                             <button type="button" class="btn btn-primary" onclick="send();">Send message</button>
			</div>	
						
		</div>
        </form>

<!-- fin MENSAJES -->


			  </div>
			
			</div>
		  </div>
		</div>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<!-- FullCalendar -->
	<script src='js/moment.min.js'></script>
	<script src='js/fullcalendar.min.js'></script>
	
	<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			//defaultDate: '2016-01-12',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd').modal('show');
			},
			eventRender: function(event, element) {
				element.bind('dblclick', function() {
					$('#ModalEdit #id').val(event.id);
					$('#ModalEdit #title').val(event.title);
					$('#ModalEdit #color').val(event.color);
					$('#ModalEdit').modal('show');
				});
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				edit(event);

			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
			events: [
			<?php foreach($events as $event): 
			
				$start = explode(" ", $event['start']);
				$end = explode(" ", $event['end']);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $event['start'];
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $event['end'];
				}
			?>
				{
					id: '<?php echo $event['id']; ?>',
					title: '<?php echo $event['title']; ?>',
					start: '<?php echo $start; ?>',
					end: '<?php echo $end; ?>',
					color: '<?php echo $event['color']; ?>',
				},
			<?php endforeach; ?>
			]
		});
		
		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			id =  event.id;
			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
			 url: 'editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){
						alert('Saved');
					}else{
						alert('Could not be saved. try again.'); 
					}
				}
			});
		}
		
	});

</script>

</body>

<script type="text/javascript" language="javascript" src="js/bootstrap-multiselect.js"></script>
<script>


function trigger_multiselect(){
    $(".multiple").multiselect({
	 buttonWidth: '100%',
	 enableCaseInsensitiveFiltering:true,
	 includeSelectAllOption: true,
	 maxHeight:400,
	 nonSelectedText: 'Select Groups',
	 selectAllText: 'Seleccionar todos'
    });


}



    $(document).ready(function() {
	
    $('#ModalEdit').on('show', function(){
        triggetmultiselect();
    });
//});
            
         //   LoadSelect2ScriptExt(DemoSelect2);
           // autocompletar_select('id_groups','group_name','id_groups',"SELECT id_groups, group_name from groups");
           // autocompletar_select('user_id','complete_name','id_user',"SELECT *,concat(Last_name,\', \',First_name) as complete_name FROM user_system  order by Last_name ");

    });


    
   // function DemoSelect2(){
	//$('#s2_with_tag').select2({placeholder: "Select Status"});	
	//$('#id_groups').select2();	
  //      //$('#id_person').select2();	
        //$('#id_user').select2();
        
//}

    function send(){
    	var id_groups=$("#id_groups").val();   
    	var id_members=$("#id_members").val();   
        var id_template=$("#id_template").val();         
        var subject=$("#subject").val();
        var sms=$("#text_date_change").val();  
        var sms1=$("#name").val();        
        
        if(subject===''){
            alert("Please enter a subject");
            return false;
        }
        
        if(id_template===undefined || id_template===''){
            
            alert("Please, select or create a template");
            return false;
        }
        if(subject===''){
            alert("Please enter a subject");
            return false;
        }
        //alert("id_members"+id_members);
        form1.submit();
    }
    
    function cargar_valores_select(valor){

	

    //   $('#table_reference').val('');
     //               if(valor <> null){ 
     //                   $('#table_reference').val('patients');
      //                  $( "#id_groups" ).load( "vista/ajax_tipo_person.php?valor="+valor);
                        
     //               }

 var valor = replaceAll(valor,' ','%20');

 						$("#id_groups").empty().html('<img src="../../../images/loader.gif" width="30" height="30"/>');
                            $("#id_groups").load("vista/ajax_tipo_person.php?valor="+valor);


                  
}

                
        function templates_n(elemento){
            //var id= elemento.selectedValue;
            var id=elemento.options[elemento.selectedIndex].value;
            
            if(id==='0'){
                
                $( "#body_sms" ).html("");
            }else{
                $( "#body_sms" ).load( "vista/message_template.php?templates=si&id_templates="+id);
                
            }

        }
        
        
         function temp(valor){
            //var id= elemento.selectedValue;
           // var valor=elemento.options[elemento.selectedIndex].value;
            //var valor = replaceAll(valor,' ','%20');
            if(valor==='0'){                
                $( "#output1" ).html("");
            }else{
                $( "#output1" ).load("vista/ajax_tipo_person.php?valor="+valor);
                
            }

        }
        
        
         

</script>


</html>

                        
    function confirmacion_almacenamiento(texto) {                                  
        swal({
          type: 'success',
          title: texto,  
          timer: 3000          
        })                                                    
    }
    
    function SoloLetras(e) {        
        tecla_codigo = (document.all) ? e.keyCode : e.which;
        if(tecla_codigo==8 || tecla_codigo==0)return true;
        patron =/[0-9]/;
        tecla_valor = String.fromCharCode(tecla_codigo);
        return patron.test(tecla_valor);
    }    

    

    function replaceAll( text, busca, reemplaza ){
        while (text.toString().indexOf(busca) != -1)
            text = text.toString().replace(busca,reemplaza);
        return text;
    }


    jQuery.fn.reset = function () {
        $(this).each (function() { this.reset(); });
    }              

    function resetear_formulario(nombre_formulario) {                
        $("#"+nombre_formulario).reset();                
    }  

                                  
    function Mayusculas(e, elemento) {
        tecla=(document.all) ? e.keyCode : e.which; 
        elemento.value = elemento.value.toUpperCase();
    }   


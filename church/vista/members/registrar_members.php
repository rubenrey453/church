         
<?php

//session_start();
require_once '../../conex.php';
//if(!isset($_SESSION['id_usuario'])){
//  header("Location: http://localhost/framework_kidworks/CODIGO_GENERADO/kidswork_therapy/mvc/vista/autenticacion/index.php?&session=finalizada", true);
    
//}
$conexion = conectar();

if(isset($_GET['id_member'])){ 
    
$accion = 'modificar';
$titulo = 'Modificar';
$generar_input = '<input type="hidden" id="id_member" Name="id_member" value="'.$_GET['id_member'].'">';

} else {
$accion = 'insertar';
$titulo = 'Registrar';
$generar_input = null;
}


?>
  <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>.: THERAPY  AID :.</title>
    


<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
<!-- Include jQuery-->

 
<!-- Include Date Range Picker -->
 
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css"/>    
    <link rel="stylesheet" type="text/css" href="../../css/sweetalert2.min.css"/>   
    <link rel="stylesheet" href="../../css/bootstrap.min.css" type="text/css"/> 
     <link rel="stylesheet" type="text/css" href="../../css/bootstrap-multiselect.css">

    <script type="text/javascript" src="../../plugins/jquery/jquery.min.js"></script>
     <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
 <script type="text/javascript" language="javascript" src="../../js/funciones.js"></script>

    <script type="text/javascript" language="javascript" src="../../js/sweetalert2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/listas.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

 <script type="text/javascript" language="javascript" src="../../js/bootstrap-multiselect.js"></script>



    <script type="text/javascript" language="javascript">

    function Validar_Formulario_Gestion_members(nombre_formulario) {              
         
    var nombres_campos = '';
    
    <?php  if(!isset($_GET['id_member'])){ ?>
                          if($('#id_member').val() == ''){
                    nombres_campos += '<table align="center" border="0" width="400px"><tr><td align="left"> * Id Member</td></tr></table>';
                                
        }
        <?php } ?>
                        
                  if($('#name').val() == ''){
                    nombres_campos += '<table align="center" border="0" width="400px"><tr><td align="left"> * Name</td></tr></table>';
                                
        }
       
           
    if(nombres_campos != ''){ 
            
        swal({
          title: "<h3><b>Complete los Siguientes Campos<b></h3>",          
          type: "info",
          html: "<h4>"+nombres_campos+"</h4>",
          showCancelButton: false,
          animation: "slide-from-top",
          closeOnConfirm: true,
          showLoaderOnConfirm: false,
        });
            
            return false; 
        
                         } else {  

                        var campos_formulario = $("#form_gestion_members").serialize();
                        
                        $.post(
                                "../../controlador/members/gestionar_members.php",
                                campos_formulario,
                                function (resultado_controlador) {
                                    mostrar_datos(resultado_controlador);
                                    resetear_formulario(nombre_formulario);
                                    
                                },
                                "json" 
                                );
                        
                        return false;
                    }
            }            
            
            function mostrar_datos(resultado_controlador) {                                         
           
            $('#resultado').html(resultado_controlador.resultado);
                      
            swal({
                title: resultado_controlador.mensaje,
                text: "",
                type: "success",
                showCancelButton: false,   
                confirmButtonColor: "#3085d6",   
                cancelButtonColor: "#d33",   
                confirmButtonText: "Aceptar",   
                closeOnConfirm: false,
                closeOnCancel: false
                }).then(function(isConfirm) {
                  if (isConfirm === false) {                    
                    $( "#conexion" ).load("consultar_members.php?&consultar=si");                    
                  }
                    });             
                                            
           }                
       
        </script>

</head>
<body>
    
   <!-- NAV BAR  -->
 <?php  include "../../nav_bar.php"; ?>
    <br><br>
    
    <div class="container">        
        <div class="row">
         
        </div>

        <div class="row">

<form id="form_gestion_members" onSubmit="return Validar_Formulario_Gestion_members('form_gestion_members');">      
        
        <div class="form-group row">                
            <div class="col-sm-2"></div>
            <div class="col-sm-10" align="left"><h3><font color="#BDBDBD"><?php echo $titulo?> members</font></h3>   </div>                  
        </div>                    
                   
        <div class="form-group row">
                           
            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Name</font></label>
            
            <div class="col-sm-8"><input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['name'])) { echo str_replace('|',' ',$_GET['name']); }?>"></div>                                                               
            </div>
           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Address</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="address" name="address" placeholder="Address" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['address'])) { echo str_replace('|',' ',$_GET['address']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">City State Zip</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="city_state_zip" name="city_state_zip" placeholder="City State Zip" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['city_state_zip'])) { echo str_replace('|',' ',$_GET['city_state_zip']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Formal Greeting</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="formal_greeting" name="formal_greeting" placeholder="Formal Greeting" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['formal_greeting'])) { echo str_replace('|',' ',$_GET['formal_greeting']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Informal Greeting</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="informal_greeting" name="informal_greeting" placeholder="Informal Greeting" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['informal_greeting'])) { echo str_replace('|',' ',$_GET['informal_greeting']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Primary Phone</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="primary_phone" name="primary_phone" placeholder="Primary Phone" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['primary_phone'])) { echo str_replace('|',' ',$_GET['primary_phone']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Home Phone</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="home_phone" name="home_phone" placeholder="Home Phone" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['home_phone'])) { echo str_replace('|',' ',$_GET['home_phone']); }?>"></div>                                                               
                                            </div>

                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Birthday</font></label>

                                            <!-- <div class="col-sm-8"><input type="text" class="form-control" id="birthday" name="birthday" placeholder="Birthday" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['birthday'])) { echo str_replace('|',' ',$_GET['birthday']); }?>"></div>                                                               
                                            </div> -->
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <div class="input-group-addon" id="birthday2" name="birthday2">
                                                        <i class="fa fa-calendar">
                                                        </i>
                                                    </div>
                                                <input class="form-control" id="birthday"  name="birthday" placeholder="MM/DD/YYYY" type="text" value="<?php if(isset($_GET['birthday'])) { echo str_replace('|',' ',$_GET['birthday']); }?>"/>
                                                </div>
                                            </div> 
                                        </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Last Name</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['last_name'])) { echo str_replace('|',' ',$_GET['last_name']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">First Name</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['first_name'])) { echo str_replace('|',' ',$_GET['first_name']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Age Bracket</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="age_bracket" name="age_bracket" placeholder="Age Bracket" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['age_bracket'])) { echo str_replace('|',' ',$_GET['age_bracket']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Age Range</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="age_range" name="age_range" placeholder="Age Range" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['age_range'])) { echo str_replace('|',' ',$_GET['age_range']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Membership Status</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="membership_status" name="membership_status" placeholder="Membership Status" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['membership_status'])) { echo str_replace('|',' ',$_GET['membership_status']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Email</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="email" name="email" placeholder="Email" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['email'])) { echo str_replace('|',' ',$_GET['email']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Marital Status</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="marital_status" name="marital_status" placeholder="Marital Status" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['marital_status'])) { echo str_replace('|',' ',$_GET['marital_status']); }?>"></div>                                                               
                                            </div> 
                                                     
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Gender</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="gender" name="gender" placeholder="Gender" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['gender'])) { echo str_replace('|',' ',$_GET['gender']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Zip</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="zip" name="zip" placeholder="Zip" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['zip'])) { echo str_replace('|',' ',$_GET['zip']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Cell Phone</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="cell_phone" name="cell_phone" placeholder="Cell Phone" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['cell_phone'])) { echo str_replace('|',' ',$_GET['cell_phone']); }?>"></div>                                                               
                                            </div>
                                           
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Work Phone</font></label>

                                            <div class="col-sm-8"><input type="text" class="form-control" id="work_phone" name="work_phone" placeholder="Work Phone" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['work_phone'])) { echo str_replace('|',' ',$_GET['work_phone']); }?>"></div>                                                               
                                            </div>
                                        <div class="form-group row">
                                                           
                                            <label class="col-sm-2"><font color="#585858">Group</font></label>

                                            <!-- <div class="col-sm-8"><input type="text" class="form-control" id="group" name="group" placeholder="group" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['group'])) { echo str_replace('|',' ',$_GET['group']); }?>"></div>                                                               
                                            </div>  -->
                                                    <div class="col-sm-8">

                                                        <select id="id_groups" name="id_groups[]" class="multiple form-control" multiple>  
                                                                                                                                                                 
                                                                <?php    
                  
                                                                $sql2  = "SELECT * FROM groups";
                                                                $resultado1 = ejecutar($sql2,$conexion); 
                                                                $j = 0;      
                                                                while($datos = mysqli_fetch_assoc($resultado1)){            
                                                                    $groups[$j] = $datos;
                                                                    $j++;
                                                                  
                                                                }
                                                             $i=0;
                                                                while(isset($groups[$i])!=null){
                                                                ?>
                                                            <option value="<?=$groups[$i]['id_groups']?>"><?=$groups[$i]['group_name']?></option>
                                                        <?php
                                                            $i++;
                                                            }

                                                        ?>
                                                        </select>
                                                    </div>  
                                                    
                                        </div>    
                           
        <div class="form-group row">
            <div class="col-sm-2" align="left"></div>
            <div class="col-sm-10" align="left"> <button type="submit" class="btn btn-primary text-left">Aceptar</button> </div>
        </div>
    <input type="hidden" id="accion" name="accion" value="<?php echo $accion?>">
    <?php echo $generar_input;?>
</form>

        </div>
    </div>


        <div id="resultado" class="text-center"></div>
        <br><br>
        <footer> 
            <div class="row"> 
                <div class="col-lg-12 text-center"> 
                    <p>&copy; Copyright &copy; THERAPY AID 2016</p> 
                </div> 
            </div> 
            <!-- /.row --> 
        </footer>
        
<script>

$(document).ready(function(){
        var date_input=$('input[name="birthday"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
//$(document).ready(function() {            
                
                $(".multiple").multiselect({
                    buttonWidth: '100%',
                  
                    enableCaseInsensitiveFiltering:true,
                    includeSelectAllOption: true,
                    maxHeight:400
                });

<?php 

if(isset($_GET['id_member'])){   
    $sql3 = "SELECT * FROM members_group_relation WHERE id_member = ".$_GET['id_member']."  "; 
                    $resultado3 = ejecutar($sql3,$conexion);
                    $reporte2 = array();
                    
                    $j=0;
                    while($datos2 = mysqli_fetch_assoc($resultado3)) {            
                       $reporte2[$j] = $datos2;

                        $j++;
        }

 echo "$('.multiple').val([";
                 for($i=0; $i<$j; $i++){
                     echo "'".$reporte2[$i]['id_groups']."'"; 
                     if($i<$j-1)
                        echo ",";
                  }
echo "]);";
    ?>

$('.multiple').multiselect("refresh");

<?php 
    } 
    ?>

    // });
    
</script>
</body>
</html>
         
<?php

//session_start();
require_once '../../conex.php';
//if(!isset($_SESSION['id_usuario'])){
//  header("Location: http://localhost/framework_kidworks/CODIGO_GENERADO/kidswork_therapy/mvc/vista/autenticacion/index.php?&session=finalizada", true);
    
//}
$conexion = conectar();

if(isset($_GET['id_group'])){ 
    
$accion = 'modificar';
$titulo = 'Modificar';
$generar_input = '<input type="hidden" id="id_group" Name="id_group" value="'.$_GET['id_group'].'">';

} else {
$accion = 'insertar';
$titulo = 'Registrar';
$generar_input = null;
}


?>
  <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>.: THERAPY  AID : Group.</title>
    


<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
<!-- Include jQuery-->

 
 
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css"/>    
    <link rel="stylesheet" type="text/css" href="../../css/sweetalert2.min.css"/>   
    <link rel="stylesheet" href="../../css/bootstrap.min.css" type="text/css"/> 

    <script type="text/javascript" src="../../plugins/jquery/jquery.min.js"></script>
     <script src="../../plugins/bootstrap/bootstrap.min.js"></script>
 <script type="text/javascript" language="javascript" src="../../js/funciones.js"></script>

    <script type="text/javascript" language="javascript" src="../../js/sweetalert2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/listas.js"></script>



    <script type="text/javascript" language="javascript">  


    function Validar_Formulario_Gestion_group(nombre_formulario) {              
         
    var nombres_campos = '';
    
    <?php  if(!isset($_GET['id_group'])){ ?>
                          if($('#id_group').val() == ''){
                    nombres_campos += '<table align="center" border="0" width="400px"><tr><td align="left"> * Id Member</td></tr></table>';
                                
        }
        <?php } ?>
                        
                  if($('#name').val() == ''){
                    nombres_campos += '<table align="center" border="0" width="400px"><tr><td align="left"> * Name</td></tr></table>';
                                
        }

           
    if(nombres_campos != ''){ 
            
        swal({
          title: "<h3><b>Complete los Siguientes Campos<b></h3>",          
          type: "info",
          html: "<h4>"+nombres_campos+"</h4>",
          showCancelButton: false,
          animation: "slide-from-top",
          closeOnConfirm: true,
          showLoaderOnConfirm: false,
        });
            
            return false; 
        
                         } else {  

                        var campos_formulario = $("#form_gestion_group").serialize();
                        
                        $.post(
                                "../../controlador/groups2/gestionar_group.php",
                                campos_formulario,
                                function (resultado_controlador) {
                                    mostrar_datos(resultado_controlador);
                                    resetear_formulario(nombre_formulario);
                                    
                                },
                                "json" 
                                );
                        
                        return false;
                    }
            }            
            
            function mostrar_datos(resultado_controlador) {                                         
           
            $('#resultado').html(resultado_controlador.resultado);
                      
            swal({
                title: resultado_controlador.mensaje,
                text: "",
                type: "success",
                showCancelButton: false,   
                confirmButtonColor: "#3085d6",   
                cancelButtonColor: "#d33",   
                confirmButtonText: "Aceptar",   
                closeOnConfirm: false,
                closeOnCancel: false
                }).then(function(isConfirm) {
                  if (isConfirm === false) {                    
                    $( "#conexion" ).load("../groups2/consultar_group.php?&consultar=si");                    
                  }else{window.location.href = "http://localhost/church/vista/groups2/consultar_group.php";   }
                    });             
                 //window.location.href = "http://localhost/church/vista/groups2/consultar_group.php";                           
           }     
       
        </script>


</head>
<body>
    
   <!-- NAV BAR  -->
 <?php  include "../../nav_bar.php"; ?>
    <br><br>

    
    <div class="container">        
        <div class="row">
         
        </div>

        <div class="row">

<form id="form_gestion_group" onSubmit="return Validar_Formulario_Gestion_group('form_gestion_group');">      
        
        <div class="form-group row">                
            <div class="col-sm-2"></div>
            <div class="col-sm-10" align="left"><h3><font color="#BDBDBD"><?php echo $titulo?> Groups</font></h3>   </div>                  
        </div>                    
                   
        <div class="form-group row">
                           
            <label class="col-sm-2 form-control-label text-right"><font color="#585858">Name</font></label>
            
            <div class="col-sm-8"><input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="Mayusculas(event, this)" value="<?php if(isset($_GET['name'])) { echo str_replace('|',' ',$_GET['name']); }?>"></div>                                                               
            </div>
           
                                    
        <div class="form-group row">
            <div class="col-sm-2" align="left"></div>
            <div class="col-sm-10" align="left"> <button type="submit" class="btn btn-primary text-left">Aceptar</button> </div>
        </div>
    <input type="hidden" id="accion" name="accion" value="<?php echo $accion?>">
    <?php echo $generar_input;?>
</form>

        </div>
    </div>


        <div id="resultado" class="text-center"></div>
        <br><br>
        <footer> 
            <div class="row"> 
                <div class="col-lg-12 text-center"> 
                    <p>&copy; Copyright &copy; THERAPY AID 2016</p> 
                </div> 
            </div> 
            <!-- /.row --> 
        </footer>
        


</script>
</body>
</html>